#!/usr/bin/php
<?php

declare(strict_types=1);

require 'vendor/autoload.php';

use judahnator\EmsiInterview\IO as IO;
use judahnator\EmsiInterview\Model\DbRecord;
use judahnator\EmsiInterview\OnetMap;
use judahnator\EmsiInterview\SocHierarchy;
use judahnator\EmsiInterview\Util\Pipeline;

// import onet
$onetMap = new OnetMap(new SplFileInfo('data/map_onet_soc.csv'));

// import soc-hierarchy
$socHierarchy = new SocHierarchy(new SplFileInfo('data/soc_hierarchy.csv'));

// create and set up the database
$dbConnector = new IO\DbConnector(new SplFileInfo('output.sqlite'));
$dbConnector->init();

// A tally of how many records we stripped HTML from
$tagsPresentIn = 0;

// Set up the data import, filtering, mapping pipeline
$pipeline = (new Pipeline(new IO\JsonReader(new SplFileInfo('data/sample.gz'))))

    // Strip HTML entities from the body
    ->forEach(static function (stdClass $record) use (&$tagsPresentIn): stdClass {
        // if the body is different without HTML tags, increment the $tagsPresentIn number
        if (($body = strip_tags($record->body)) !== $record->body) {
            $tagsPresentIn++;
            $record->body = $body;
        }
        return $record;
    })

    // inject soc5 and soc2
    ->forEach(static function (stdClass $record) use ($onetMap, $socHierarchy): stdClass {
        $record->soc5 = $socHierarchy->getRecord($soc5 = $onetMap->getSoc5($record->onet));
        $record->soc2 = $socHierarchy->getParentOf($soc5, 2);
        return $record;
    })

    // map into the DbRecord model
    ->forEach(static function (stdClass $record): DbRecord {
        return new DbRecord(
            $record->body,
            $record->title,
            DateTime::createFromFormat('Y-m-d', $record->expired),
            DateTime::createFromFormat('Y-m-d', $record->posted),
            $record->state,
            $record->city,
            $record->onet,
            $record->soc5,
            $record->soc2,
        );
    })

    // chunk into groups of 100 for easier insertion
    ->forAll(static function (iterable $records): Generator {
        $chunk = [];
        foreach ($records as $record) {
            $chunk[] = $record;
            if (count($chunk) === 100) {
                yield $chunk;
                $chunk = [];
            }
        }
        yield $chunk;
    })

    // For each of the chunked groups, save them and return the saved count
    ->forEach(static function (array $chunked) use ($dbConnector): int {
        $dbConnector->addRecords(...$chunked);
        return count($chunked);
    });

// note how many records we are saving
$total = 0;

// This iteration runs the pipeline, saving the records and returning the numer saved in each chunk
foreach ($pipeline as $recordsSaved) {
    $total += $recordsSaved;
}

echo "Records imported: {$total}", PHP_EOL;
echo "Documents where HTML was removed: {$tagsPresentIn}", PHP_EOL;
