Running Locally
===============

If you are not already setup in the PHP ecosystem then running locally can be a bit of a pain. To help I added a `install.sh` file that should get you up and running.

I tested this file with a fresh install of Debian Stretch on a LXC container with 8GB of disk space, 2 cores, and 2048MiB of both memory and swap.

Please for the love of everything that is good in the world, review the script before running it. It's not good to blindly trust bash scripts from strangers on the internet. This is especially the case because the script needs to run as root to install third party software.

After the script runs there should be a new `emsi-import-project-php/` directory which contains the application, and in this directory there should be an `output.sqlite` file to review

Data Sanitization Notes
=======================

I noticed one or more SOC numbers that were present in the sample data that were missing or omitted from the SOC files. For example, 13-1023 and 53-1031.

To compensate for this problem I wrote a system to adjust the least-significant non-zero digit to zero and search for that instead, as that seems to be the next-best item in the hierarchy list. This process would repeat until we were either at soc2 or found a candidate. For example, 13-1023 would become 13-1020 then 13-1000.

I should clarify that this is not something I would do in production. I would much rather fill in the missing data than try to guess.

Reports
=======

There were two reports whose functionality I did not bake into the application. I likely could have, but didnt want to blow over my 8 hour budget by making everything perfect.

The implementation requirements did mention that I could leave these as documented SQL queries, so here they are:

Count of each soc2
------------------

```sql
SELECT soc2, COUNT(*) 
FROM records 
GROUP BY soc2 
ORDER BY soc2;
```

This is a simple enough query. It gets a list of the soc2's and creates an aggregate with a count.

Postings active Feb 1st, 2017
-----------------------------

```sql
SELECT COUNT(*) 
FROM records 
WHERE 
  posted <= '2017-02-01' and 
  expired > '2017-02-01';
```

This one selects a count of the records that had been posted prior to or on that date, and that expired after that date.

I took "expired" to be an inclusive-date, so I did not include the items that expired on that day in the tally.