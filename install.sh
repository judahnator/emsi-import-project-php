#!/usr/bin/env bash

# Install prerequisites for PHP + git & sqlite & wget for later
apt-get update && apt-get -y install lsb-release apt-transport-https ca-certificates git sqlite wget
wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | tee /etc/apt/sources.list.d/php.list

# install PHP and extensions
apt-get update && apt-get -y install php7.4-cli php7.4-json php7.4-pdo php7.4-sqlite3 php7.4-zip

# install composer (php package manager)
# dont go piping wget to an executable normally, do as I say not as I do.
wget https://raw.githubusercontent.com/composer/getcomposer.org/master/web/installer -O - -q | php -- --quiet
chmod +x composer.phar && mv composer.phar /usr/bin/composer
COMPOSER_ALLOW_SUPERUSER=1

# grab the git repository
git clone --recurse-submodules https://gitlab.com/judahnator/emsi-import-project-php.git

# Install the various composer dependencies
cd emsi-import-project-php && composer install --no-dev

# run the application
php app.php