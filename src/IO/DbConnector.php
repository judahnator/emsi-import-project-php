<?php

declare(strict_types=1);

namespace judahnator\EmsiInterview\IO;

use judahnator\EmsiInterview\Model\DbRecord;
use PDO;
use SplFileInfo;

/**
 * Class DbConnector
 *
 * Basic database abstraction.
 *
 * @package judahnator\EmsiInterview\IO
 */
final class DbConnector
{
    private PDO $db;

    public function __construct(SplFileInfo $dbLocation)
    {
        $this->db = new PDO("sqlite:{$dbLocation}");
    }

    /**
     * Drops the "records" table if it exists, then re-creates it.
     */
    public function init(): void
    {
        $this->db->exec(
            <<<SQL
            DROP TABLE IF EXISTS records;
            SQL
        );
        $this->db->exec(
            <<<SQL
            CREATE TABLE records (
                id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                body TEXT NOT NULL,
                title TEXT NOT NULL,
                expired DATE NOT NULL,
                posted DATE NOT NULL,
                state TEXT NOT NULL,
                city TEXT NOT NULL,
                onet TEXT NOT NULL,
                soc5 TEXT NOT NULL,
                soc2 TEXT NOT NULL
            ); 
            SQL
        );
    }

    /**
     * Given a number of records, prepare a query then insert them all in a single transaction.
     *
     * @param DbRecord ...$records
     */
    public function addRecords(DbRecord ...$records): void
    {
        $query = $this->db->prepare("INSERT INTO records (body, title, expired, posted, state, city, onet, soc5, soc2) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);");
        $this->db->beginTransaction();
        foreach ($records as $record) {
            $query->execute([
                $record->getBody(),
                $record->getTitle(),
                $record->getExpired()->format('Y-m-d'),
                $record->getPosted()->format('Y-m-d'),
                $record->getState(),
                $record->getCity(),
                $record->getOnet(),
                $record->getSoc5()->getChild(),
                $record->getSoc2()->getChild(),
            ]);
        }
        $this->db->commit();
    }
}
