<?php

declare(strict_types=1);

namespace judahnator\EmsiInterview\IO;

use Generator;

/**
 * Class JsonReader
 *
 * This file reader will attempt to provide a JSON object for every line in the document.
 *
 * @package judahnator\EmsiInterview\IO
 */
final class JsonReader extends FileReader
{
    public function getIterator(): Generator
    {
        foreach (parent::getIterator() as $line) {
            yield json_decode($line, false, 2, JSON_THROW_ON_ERROR);
        }
    }
}
