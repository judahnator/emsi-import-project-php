<?php

declare(strict_types=1);

namespace judahnator\EmsiInterview\IO\Interpreter;

use Generator;
use SplFileInfo;

/**
 * Class Gz
 *
 * Reads a gzipped file line by line.
 *
 * @package judahnator\EmsiInterview\IO\Interpreter
 */
final class Gz implements Interpreter
{
    public static function readLinesFrom(SplFileInfo $file): Generator
    {
        $fileHandle = gzopen($file->getRealPath(), 'r');
        while (!gzeof($fileHandle) && ($line = gzgets($fileHandle)) !== false) {
            yield $line;
        }
        gzclose($fileHandle);
    }
}
