<?php

declare(strict_types=1);

namespace judahnator\EmsiInterview\IO\Interpreter;

use Generator;
use SplFileInfo;

/**
 * Class Plaintext
 *
 * Reads a plain-text file line by line.
 *
 * @package judahnator\EmsiInterview\IO\Interpreter
 */
final class Plaintext implements Interpreter
{
    public static function readLinesFrom(SplFileInfo $file): Generator
    {
        $fileHandle = fopen($file->getRealPath(), 'r');
        while (!feof($fileHandle) && ($line = fgets($fileHandle)) !== false) {
            yield $line;
        }
        fclose($fileHandle);
    }
}
