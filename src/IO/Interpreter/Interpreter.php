<?php

declare(strict_types=1);

namespace judahnator\EmsiInterview\IO\Interpreter;

use Generator;
use SplFileInfo;

/**
 * Interface Interpreter
 *
 * Describes a class that will read a file line by line.
 *
 * @package judahnator\EmsiInterview\IO\Interpreter
 */
interface Interpreter
{
    public static function readLinesFrom(SplFileInfo $file): Generator;
}
