<?php

declare(strict_types=1);

namespace judahnator\EmsiInterview\IO;

use Generator;

/**
 * Class CsvReader
 *
 * Parses a CSV file, creating a generator that pairs the header keys with each rows values.
 *
 * @package judahnator\EmsiInterview\IO
 */
final class CsvReader extends FileReader
{
    public function getIterator(): Generator
    {
        $firstRow = true;
        $headers = [];
        foreach (parent::getIterator() as $line) {
            $parsedLine = str_getcsv($line);
            if ($firstRow) {
                $firstRow = false;
                $headers = $parsedLine;
                continue;
            }
            yield array_combine($headers, $parsedLine);
        }
    }
}
