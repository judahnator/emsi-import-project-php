<?php

declare(strict_types=1);

namespace judahnator\EmsiInterview\IO;

use Generator;
use IteratorAggregate;
use judahnator\EmsiInterview\IO\Interpreter\Gz;
use judahnator\EmsiInterview\IO\Interpreter\Plaintext;
use RuntimeException;
use SplFileInfo;

/**
 * Class FileReader
 *
 * Takes a given file and provides an iterable interface to read it line by line.
 *
 * @package judahnator\EmsiInterview\IO
 */
abstract class FileReader implements IteratorAggregate
{
    // The file extensions we know how to read and the interpreters to use for them
    private const INTERPRETERS = [
        'gz' => Gz::class,
        'csv' => Plaintext::class,
    ];

    protected SplFileInfo $file;

    public function __construct(SplFileInfo $file)
    {
        if (!$file->isReadable()) {
            throw new RuntimeException("Cannot read from this file.");
        }
        $this->file = $file;
    }

    public function getIterator(): Generator
    {
        if (!array_key_exists($ext = $this->file->getExtension(), self::INTERPRETERS)) {
            throw new RuntimeException("Unknown file extension provided.");
        }
        yield from (self::INTERPRETERS[$ext])::readLinesFrom($this->file);
    }
}
