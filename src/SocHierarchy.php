<?php

declare(strict_types=1);

namespace judahnator\EmsiInterview;

use InvalidArgumentException;
use judahnator\EmsiInterview\IO\CsvReader;
use judahnator\EmsiInterview\Model\Soc;
use SplFileInfo;

/**
 * Class SocHierarchy
 *
 * This class acts as a helper to parse the soc hierarchy file.
 * It will hold all the records in a hash-map and assist in fetching records and their parents.
 *
 * @package judahnator\EmsiInterview
 */
final class SocHierarchy
{
    private array $records = [];

    public function __construct(SplFileInfo $mapHierarchyFile)
    {
        foreach (new CsvReader($mapHierarchyFile) as ['child' => $child, 'parent' => $parent, 'level' => $level, 'name' => $name]) {
            $this->records[$child] = new Soc($child, $parent, (int)$level, $name);
        }
    }

    /**
     * Given a soc number, attempt to find its parent.
     * If no $level is provided only the direct parent will be returned.
     * If $level is an integer between 1 and 5 inclusive, it will return the first parent at or above that level.
     *
     * @param string $childSoc
     * @param int|null $level
     * @return Soc
     */
    public function getParentOf(string $childSoc, int $level = null): Soc
    {
        // input validation
        if (!is_null($level) && ($level < 0 || $level > 5)) {
            throw new InvalidArgumentException("The level provided must be 1-5.");
        }

        // if no level is provided this will iterate once and get the direct parent.
        // if a level is provided, it will iterate until we are at that level.
        $record = $this->getRecord($childSoc);
        do {
            $record = $this->getRecord($record->getParent());
        } while (is_null($level) || $record->getLevel() > $level);

        return $record;
    }

    /**
     * Given a soc number, attempt to find and return the appropriate soc object from the map.
     *
     * @param string $soc
     * @return Soc
     */
    public function getRecord(string $soc): Soc
    {
        // We could not find this soc.
        if (!array_key_exists($soc, $this->records)) {
            // This will be the right-most digit
            $lsn = rtrim(substr($soc, 3), '0');

            // If the lsn is empty then it was all zeros, and we could not find a nearby soc
            if (empty($lsn)) {
                throw new InvalidArgumentException("Cannot find soc with id: {$soc}.");
            }

            // Attempt to find a similar soc in the same field by changing the greatest
            // non-zero least significant digit to zero.
            // for example, 12-3456 => 12-3450 => 12-3400 => 12-3000 => 12-0000
            return $this->getRecord(
                substr($soc, 0, 3) . str_pad(substr($lsn, 0, -1), 4, '0')
            );
        }
        return $this->records[$soc];
    }
}
