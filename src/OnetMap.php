<?php

declare(strict_types=1);

namespace judahnator\EmsiInterview;

use judahnator\EmsiInterview\IO\CsvReader;
use SplFileInfo;

/**
 * Class OnetMap
 *
 * This class helps extract the soc information from the onet mapping file.
 *
 * @package judahnator\EmsiInterview
 */
final class OnetMap
{
    private array $records = [];

    public function __construct(SplFileInfo $mapLocation)
    {
        foreach (new CsvReader($mapLocation) as ['onet' => $onet, 'soc5' => $soc5]) {
            $this->records[$onet] = $soc5;
        }
    }

    public function getSoc5(string $onet): ?string
    {
        return $this->records[$onet] ?? null;
    }
}
