<?php

declare(strict_types=1);

namespace judahnator\EmsiInterview\Util;

use Generator;
use IteratorAggregate;

/**
 * Class Pipeline
 *
 * A helper that will take an iterable and take a user-defined action on it when iterated on.
 *
 * @package judahnator\EmsiInterview\Util
 */
final class Pipeline implements IteratorAggregate
{
    private iterable $input;
    private $then;

    /**
     * Pipeline constructor.
     * Will accept any iterable and an optional callback.
     *
     * @param iterable $input
     * @param callable|null $then
     */
    public function __construct(iterable $input, callable $then = null)
    {
        $this->input = $input;
        $this->then = $then;
    }

    /**
     * Applies the callback to the input and returns the new iterable object.
     *
     * @return Generator
     */
    public function getIterator(): Generator
    {
        if (is_null($this->then)) {
            yield from $this->input;
            return;
        }

        yield from ($this->then)($this->input);
    }

    /**
     * Applies a given callback to the entire iterator.
     *
     * @param callable $then
     * @return self
     */
    public function forAll(callable $then): self
    {
        return new self($this, $then);
    }

    /**
     * Applies a callback to each of the iterable items in turn.
     *
     * @param callable $then
     * @return self
     */
    public function forEach(callable $then): self
    {
        return $this->forAll(
            static function (iterable $input) use ($then): Generator {
                foreach ($input as $k => $v) {
                    yield $then($v, $k);
                }
            }
        );
    }
}
