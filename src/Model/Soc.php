<?php

declare(strict_types=1);

namespace judahnator\EmsiInterview\Model;

/**
 * Class Soc
 *
 * A model to hold basic soc info.
 *
 * @package judahnator\EmsiInterview\Model
 */
final class Soc
{
    private string $child;
    private string $parent;
    private int $level;
    private string $name;

    public function __construct(string $child, string $parent, int $level, string $name)
    {
        $this->child = $child;
        $this->parent = $parent;
        $this->level = $level;
        $this->name = $name;
    }

    public function getChild(): string
    {
        return $this->child;
    }

    public function getParent(): string
    {
        return $this->parent;
    }

    public function getLevel(): int
    {
        return $this->level;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
