<?php

declare(strict_types=1);

namespace judahnator\EmsiInterview\Model;

use DateTime;

/**
 * Class DbRecord
 *
 * A structure to hold the database records.
 *
 * @package judahnator\EmsiInterview\Model
 */
final class DbRecord
{
    private string $body;
    private string $title;
    private DateTime $expired;
    private DateTime $posted;
    private string $state;
    private string $city;
    private string $onet;
    private Soc $soc5;
    private Soc $soc2;

    public function __construct(string $body, string $title, DateTime $expired, DateTime $posted, string $state, string $city, string $onet, Soc $soc5, Soc $soc2)
    {
        $this->body = $body;
        $this->title = $title;
        $this->expired = $expired;
        $this->posted = $posted;
        $this->state = $state;
        $this->city = $city;
        $this->onet = $onet;
        $this->soc5 = $soc5;
        $this->soc2 = $soc2;
    }

    public function getBody(): string
    {
        return $this->body;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getExpired(): DateTime
    {
        return $this->expired;
    }

    public function getPosted(): DateTime
    {
        return $this->posted;
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getOnet(): string
    {
        return $this->onet;
    }

    public function getSoc5(): Soc
    {
        return $this->soc5;
    }

    public function getSoc2(): Soc
    {
        return $this->soc2;
    }
}
